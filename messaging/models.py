from django.db import models

class Client(models.Model):
    phone_number = models.CharField(max_length=12, unique=True)
    mobile_operator_code = models.CharField(max_length=10, blank=True, null=True)
    tag = models.CharField(max_length=50, blank=True, null=True)
    time_zone = models.CharField(max_length=50, blank=True, null=True)

class Message(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50)
    campaign = models.ForeignKey('Campaign', on_delete=models.CASCADE)
    client = models.ForeignKey('Client', on_delete=models.CASCADE)

class Campaign(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    message_text = models.TextField()
    client_filter_mobile_operator_code = models.CharField(max_length=10, blank=True, null=True)
    client_filter_tag = models.CharField(max_length=50, blank=True, null=True)