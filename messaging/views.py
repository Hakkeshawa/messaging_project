from rest_framework import generics
from rest_framework.response import Response
from .models import Client, Message, Campaign, models
from django.http import JsonResponse
from .serializers import ClientSerializer, MessageSerializer, CampaignSerializer
import requests

class ClientListCreateView(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class ClientUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class CampaignListCreateView(generics.ListCreateAPIView):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer

class CampaignUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer

class MessageStatisticsView(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def list(self, request, *args, **kwargs):
        total_campaigns = Campaign.objects.count()
        total_messages = Message.objects.count()
        message_status_counts = Message.objects.values('status').annotate(count=models.Count('id'))

        statistics = {
            'total_campaigns': total_campaigns,
            'total_messages': total_messages,
            'message_status_counts': message_status_counts
        }

        return Response(statistics)

class CampaignMessagesView(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def get_queryset(self):
        campaign_id = self.kwargs['campaign_id']
        return Message.objects.filter(campaign_id=campaign_id)

    def send_message(self, campaign, client):
        external_server_url = "https://probe.fbrq.cloud/v1"

        payload = {
            "campaign_id": campaign.id,
            "client_id": client.id,
            "message_text": "Your message text here",
            # Add other required parameters
        }

        try:
            response = requests.post(external_server_url, json=payload)

            if response.status_code == 200:
                print("Message sent successfully")
            else:
                print(f"Failed to send message. Status code: {response.status_code}")

        except requests.exceptions.RequestException as e:
            print(f"Error making request: {e}")

        new_message_status = "Sent" if response.status_code == 200 else "Failed"
        new_message = Message(status=new_message_status, campaign_id=campaign.id, client_id=client.id)
        new_message.save()
