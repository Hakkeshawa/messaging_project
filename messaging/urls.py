from django.urls import path
from .views import (
    ClientListCreateView, ClientUpdateDeleteView,
    CampaignListCreateView, CampaignUpdateDeleteView,
    MessageStatisticsView, CampaignMessagesView
)

urlpatterns = [
    path('clients/', ClientListCreateView.as_view(), name='client-list-create'),
    path('clients/<int:pk>/', ClientUpdateDeleteView.as_view(), name='client-update-delete'),
    path('campaigns/', CampaignListCreateView.as_view(), name='campaign-list-create'),
    path('campaigns/<int:pk>/', CampaignUpdateDeleteView.as_view(), name='campaign-update-delete'),
    path('messages/statistics/', MessageStatisticsView.as_view(), name='message-statistics'),
    path('messages/<int:campaign_id>/', CampaignMessagesView.as_view(), name='campaign-messages'),
]